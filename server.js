require("dotenv").config();
const express = require("express");
const cors = require("cors");
const port = process.env.PORT || 10101;
const app = express();

app.use(express.json());
app.use(
  express.urlencoded({
    extended: true,
  })
);
app.use(cors());

const shortyRoute = require("./routes/shorty");
app.use("/shortyApi", shortyRoute);

app.listen(port, () => console.log(`Listening on port ${port}`));
