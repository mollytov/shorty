const express = require("express");
const router = express();
const { insertShorty, getLong } = require("../utils/db_utils");

// Adds a new shortened URL record
router.post("/addShorty", async (req, res) => {
  const long = req.body.long;
  try {
    new URL(long); // throws an error if the url isn't valid
    const result = await insertShorty(long);
    res.send(result);
  } catch (err) {
    if (err.code === "ERR_INVALID_URL") {
      res.send("Invalid URL");
    } else {
      console.log(err);
      res.send(err);
    }
  }
});

// Adds a new shortened URL record
router.post("/addCustomShorty", async (req, res) => {
  console.log(req.body);
  const short = req.body.short;
  const long = req.body.long;
  try {
    const result = await insertShorty(short, long);
    console.log(result);
    res.send(result);
  } catch (err) {
    console.log(err);
    res.send(err);
  }
});

// Redirects user to the long URL
router.get("/go/:short", async (req, res) => {
  const short = req.params.short;
  const result = await getLong(short);
  res.redirect(result);
});

module.exports = router;
