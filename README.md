# Shorty
- [Shorty](#shorty)
  - [Requirements](#requirements)
  - [Usage](#usage)
  - [Explanation:](#explanation)
    - [What is shorty?](#what-is-shorty)
    - [Project Structure](#project-structure)
    - [Postgres DB Schema](#postgres-db-schema)
    - [Brief Server Rundown](#brief-server-rundown)

## Requirements

- yarn (`npm install -g yarn`)
- nvm (https://github.com/nvm-sh/nvm): Recommended to manage your node version, I think yarn requires 12+ these days.
- a postgres database (setup beyond the scope of this readme, schema explained [below](#postgres-db-schema))

## Usage

- populate your `db_credentials.json` with your postgres connection info
- edit your `.env` and `client/.env` with where your API is hosted
- start live server: `node server.js`
- local/test environment: `yarn dev`

## Explanation:

### What is shorty?

"Shorty" is a full PERN (PostgresQL, express.js, react.js, node.js) stack URL shortener application in the vein of [bit.ly](https://bit.ly). I started this as a code challenge for an interview and decided to polish it up a little to put on [my website](https://mollys.space/shorty). The react app was created with create-react-app, and the express backend is adapted from a skeleton I've used for many such projects.

### Project Structure

```
.shorty
├── client
│  ├── build
│  ├── package.json
│  ├── public
│  └── src
├── routes
│  └── shorty.js
├── utils
│  └── db_utils.js
├── package.json
└── server.js
```

- The root directory is the backend
- `./client` directory holds the react app
- API routes are in `./routes`
- Database queries and other misc are in `./utils`
- `client/build` has the static files to be hosted after `yarn build`ing

### Postgres DB Schema

At the moment, there's just one table with three columns:

- `"id"`: an auto-incrementing serial int
- `"hash"`: the shortened URL slug, which is a hash of the id
- `"url"`: the full URL to which the user is directed upon visiting `https://mollys.space/shortyApi/go/<short>`

### Brief Server Rundown

- https://mollys.space is (as of 2022-03-01) an Amazon EC2 free tier instance, with nginx setup as a reverse proxy.
- The node.js server runs on localhost, and requests to `/shortyApi` are forwarded to it.
- The react app is built and hosted statically, served on `/shorty` through nginx.
