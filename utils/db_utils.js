const { Pool } = require("pg");
const Hashids = require("hashids");
const credentials = require("../db_credentials.json");

const pool = new Pool(credentials);
pool.connect();

/**
 * Creates a new shortened url pairing
 * @param {string} short
 * @param {string} long
 * @returns {Promise<string>} copyable shorty URL
 */
async function insertShorty(long) {
  try {
    // the id column is a sequence which auto increments, so if we insert a url first,
    // we can get back a unique id
    const result = await pool.query(
      `INSERT INTO shorty (url) VALUES ($1) RETURNING id`,
      [long]
    );
    const hash = new Hashids("shorty");
    const hashedId = hash.encode(result.rows[0].id);
    // we hash the id to generate a smaller string, then update the hash column with it
    await pool.query(`UPDATE shorty SET hash = $1 WHERE id = $2`, [
      hashedId,
      result.rows[0].id,
    ]);
    return `${process.env.API_ROOT}/go/${hashedId}`;
  } catch (err) {
    console.log(err);
    return err;
  }
}

// TODO: make this work
/*
 * Creates a new shortened url pairing
 * @param {string} short
 * @param {string} long
 * @returns {Promise<string>} copyable shorty URL
 */
async function insertCustomShorty(short, long) {
  try {
    const result = await pool.query(
      `INSERT INTO shorty_old (short, long) VALUES ($1, $2)`,
      [short, long]
    );
    if (result.rowCount > 0) {
      return `${process.env.API_ROOT}/go/${short}`;
    } else {
      throw new Error("Couldn't insert.");
    }
  } catch (err) {
    if (err.code === "23505") {
      const result = await getShort(long);
      return result;
    } else {
      console.log(err);
      return "Invalid or duplicate shorty requested.";
    }
  }
}

/**
 * Retrieves the long URL given a shortened slug
 * @param {string} short
 * @returns {Promise<string>} URL that matches short
 */
async function getLong(short) {
  const result = await pool.query(`SELECT (url) FROM shorty WHERE hash = $1`, [
    short,
  ]);
  console.log(result);
  if (result.rowCount > 0) {
    return result.rows[0].url;
  } else {
    return "/shorty";
  }
}

/**
 * Retrieves the shortened URL given a long
 * @param {string} long
 * @returns {Promise<string>} URL that matches short
 */
async function getShort(long) {
  const result = await pool.query(`SELECT (hash) FROM shorty WHERE long = $1`, [
    long,
  ]);
  if (result.rowCount > 0) {
    return result.rows[0].short;
  } else {
    return null;
  }
}

module.exports = {
  insertShorty,
  insertCustomShorty,
  getLong,
  getShort,
};
