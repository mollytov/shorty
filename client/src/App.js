import React from "react";
import Shorty from "./components/shorty";
import CssBaseline from "@material-ui/core/CssBaseline";
import "./index.css";

const App = () => {
  return (
    <CssBaseline>
      <Shorty />
    </CssBaseline>
  );
};

export default App;
