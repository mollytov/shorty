import React from "react";
import { Grid, Typography, Button } from "@material-ui/core";

const Result = (props) => {
  return props.result === 1 ? (
    <Typography variant="body1">Invalid URL, try again!</Typography>
  ) : props.result === 2 ? (
    <Typography variant="body1">
      Something went wrong with the backend :(
    </Typography>
  ) : (
    props.result && (
      <Grid container spacing={3}>
        <Grid
          item
          style={{
            display: "flex",
            flexDirection: "column",
            justifyContent: "center",
          }}
        >
          <Typography variant="body1">
            Your shortened URL:{" "}
            <a href={props.result} target="_blank" rel="noopener noreferrer">
              {props.result}
            </a>
          </Typography>
        </Grid>
        <Grid item>
          <Button
            onClick={() => {
              navigator.clipboard.writeText(props.result);
            }}
            variant="outlined"
            style={{ textTransform: "none" }}
          >
            Copy to clipboard
          </Button>
        </Grid>
      </Grid>
    )
  );
};

export default Result;
