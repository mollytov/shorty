import React, { useState } from "react";
import axios from "axios";
import Form from "./form";
import Result from "./result";
import { Grid, Typography } from "@material-ui/core";

const Shorty = () => {
  const [shortyResult, setShortyResult] = useState(null);
  const [submitting, setSubmitting] = useState(false);
  const submitShorty = async (data) => {
    console.log(data);
    // throttle submissions to 1 per second
    if (submitting) {
      return;
    }
    setSubmitting(true);
    setTimeout(() => {
      setSubmitting(false);
    }, 1000);

    // POST to the backend
    const apiResult = await axios.post(
      `${process.env.REACT_APP_API_ROOT}/addShorty`,
      { long: data.long }
    );
    // We're validating the URL on the backend, so we want to set the result depending on the result of validation
    // It'd probably be better to validate the URL client side first,
    // and also have an error state variable rather than doing a string comparison
    if (apiResult.status < 400 && apiResult.data !== "Invalid URL") {
      // POST worked & is a valid URL
      setShortyResult(apiResult.data);
    } else if (apiResult.status < 400) {
      // POST worked but it's not a valid URL
      setShortyResult(1);
    } else {
      // POST erred
      setShortyResult(2);
    }
  };
  return (
    <Grid
      container
      justifyContent="center"
      alignItems="center"
      spacing={5}
      direction="column"
      style={{ minHeight: "100vh" }}
    >
      <Grid item>
        <Typography variant="h3">Shorty</Typography>
      </Grid>
      <Grid item>
        <Typography variant="h4">
          Put in a URL, get out a shorter URL. Magic!
        </Typography>
      </Grid>
      <Grid item>
        <Form submitShorty={submitShorty} />
      </Grid>
      <Grid item>
        <Result result={shortyResult} />
      </Grid>
      <Typography>
        See how it works on my gitlab{" "}
        <a
          href="https://gitlab.com/mollytov/shorty"
          target="_blank"
          rel="noopener noreferrer"
        >
          here.
        </a>
      </Typography>
    </Grid>
  );
};

export default Shorty;
