import React from "react";
import { useForm, Controller } from "react-hook-form";
import { Button, TextField, Grid } from "@material-ui/core";
const Form = (props) => {
  const { control, handleSubmit } = useForm({ defaultValues: { long: "" } });

  return (
    <form onSubmit={handleSubmit(props.submitShorty)}>
      <Grid container spacing={3}>
        <Grid item>
          <Controller
            name="long"
            control={control}
            render={({ field }) => (
              <TextField
                variant="outlined"
                label="URL to shorten"
                fullWidth
                {...field}
                style={{ minWidth: "50vh" }}
              />
            )}
          />
        </Grid>
        <Grid item>
          <Button
            variant="outlined"
            type="submit"
            style={{ minHeight: "100%", textTransform: "none" }}
          >
            Make it short!
          </Button>
        </Grid>
      </Grid>
    </form>
  );
};

export default Form;
